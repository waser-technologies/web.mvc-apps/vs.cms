from cms.toolbar_base import CMSToolbar
from cms.toolbar_pool import toolbar_pool
from cms.toolbar.items import LinkItem
from cms.cms_toolbars import LANGUAGE_MENU_IDENTIFIER

from django.utils.translation import override, gettext_lazy as _