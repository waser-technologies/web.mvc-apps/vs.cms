from django import forms
from djangocms_icon.fields import IconField
from djangocms_link.forms import LinkForm
from djangocms_bootstrap4.contrib.bootstrap4_grid.models import Bootstrap4GridContainer
from djangocms_bootstrap4.contrib.bootstrap4_grid.forms import Bootstrap4GridRowForm
from django.utils.translation import gettext_lazy as _

from .models import LinkItem, FooterListItemModel, FooterListItemLinkModel, FooterPageLinkItemModel, FooterSocialIconItemModel, ColumnLayoutRow, SectionModel
from .constants import SECTION_DEFAULT_TAG, SECTION_DEFAULT_ATTRIUTES, SECTION_INVERTED_ATTRIUTES

class RowPluginForm(Bootstrap4GridRowForm):
    layout = forms.ModelChoiceField(label=_("Column layout"), queryset=ColumnLayoutRow.objects.all(), widget=forms.RadioSelect(), required=False)

    def __init__(self, *args, **kwargs):
        super(RowPluginForm, self).__init__(*args, **kwargs)
        self.fields['layout'].empty_label = _("Empty")

class SectionPluginForm(forms.ModelForm):
    margin = forms.BooleanField(initial=True, required=False, help_text=_("Creates a container inside the section."))
    inverted = forms.BooleanField(initial=False, required=False, help_text=_("Should this section be inverted ?"))
    
    class Meta:
        model = SectionModel
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        super(SectionPluginForm, self).__init__(*args, **kwargs)
        self.fields['tag_type'].initial = SECTION_DEFAULT_TAG

    def save(self, commit=True):
        form = super(SectionPluginForm, self).save(commit=False)
        if self.is_valid():
            is_inverted = self.cleaned_data.get('inverted')
            
            if not is_inverted:
                attrs = SECTION_DEFAULT_ATTRIUTES
            else:
                attrs = SECTION_INVERTED_ATTRIUTES
            form.attributes = attrs

        if commit:
            form.save()

        return form


class LinkItemForm(LinkForm):

    class Meta:
        model = LinkItem
        fields = ('text', 'external_link', 'internal_link', 'target', 'attributes', 'anchor', 'mailto', 'phone')

class FooterListItemForm(forms.ModelForm):
    icon = IconField(required=True)

    class Meta:
        model = FooterListItemModel
        fields = ('icon', 'text')

class FooterListItemLinkForm(LinkForm):
    icon = IconField(required=True)

    class Meta:
        model = FooterListItemLinkModel
        fields = ('icon', 'text', 'external_link', 'internal_link', 'target', 'attributes', 'anchor', 'mailto', 'phone')

class FooterPageLinkItemForm(LinkForm):
    class Meta:
        model = FooterPageLinkItemModel
        fields = ('name', 'external_link', 'internal_link', 'target', 'attributes')

class FooterSocialIconItemForm(forms.ModelForm):
    icon = IconField(required=True)

    class Meta:
        model = FooterSocialIconItemModel
        fields = ('icon', 'link')