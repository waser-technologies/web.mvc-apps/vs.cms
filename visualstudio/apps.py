from django.apps import AppConfig


class VSConfig(AppConfig):
    name = 'visualstudio'
