from django.utils.translation import gettext_lazy as _
import os.path

DSS = {
    'settings': {
        'CMS_TEMPLATES': [
            ## Customize this
            ('visualstudio/pages/_page.html', _('Blank page')),
            ('visualstudio/pages/_page_footer.html', _('Blank page with Footer')),
        ],
        'DJANGOCMS_ICON_SETS': [
            ('fontawesome5regular', 'far', 'Font Awesome Regular'),
            ('fontawesome5solid', 'fas', 'Font Awesome 5 Solid'),
            ('fontawesome5brands', 'fab', 'Font Awesome 5 Brands'),
        ],
        'CKEDITOR_SETTINGS': {
            'language': '{{ language }}',
            'stylesSet': 'default:/static/js/addons/ckeditor.wysiwyg.js',
            'contentsCss': [
                '/static/djangocms_text_ckeditor/ckeditor/contents.css',
                '/static/djangocms_text_ckeditor/ckeditor/plugins/copyformatting/styles/copyformatting.css',
                'https://use.fontawesome.com/releases/v5.3.1/css/all.css',
            ],
        },
        'SECTION_DEFAULT_ATTRIBUTES': "{'class': 'section py-4'}",
        'SECTION_INVERTED_ATTRIBUTES': "{'class': 'section section-inverted py-4'}",
    },
}