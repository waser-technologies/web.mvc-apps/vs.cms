from django.db import models
from django.utils.translation import gettext_lazy as _

from cms.models import CMSPlugin
from filer.fields.image import FilerImageField, FilerFileField
from djangocms_link.models import AbstractLink
from djangocms_icon.fields import Icon
from djangocms_bootstrap4.fields import AttributesField
from djangocms_bootstrap4.contrib.bootstrap4_grid.models import Bootstrap4GridColumn
from djangocms_bootstrap4.helpers import mark_safe_lazy
from djangocms_bootstrap4.contrib.bootstrap4_grid.constants import GRID_CONTAINER_CHOICES
from djangocms_bootstrap4.fields import (
    AttributesField, TagTypeField,
)

# Create your models here.
class BackgroundImage(CMSPlugin):
    image = FilerImageField(verbose_name=_('image'), blank=False, null=False, related_name='image', on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Background image')
        verbose_name_plural = _('Background image')

class LinkItem(AbstractLink):
    text = models.CharField(verbose_name=_("Text"), max_length=64)

    class Meta:
        verbose_name = _("Link item")
        verbose_name_plural = _("Link items")

class FooterListItemModel(CMSPlugin):
    icon = Icon()
    text = models.TextField(verbose_name=_("Text"))

    class Meta:
        verbose_name = _("List item")
        verbose_name_plural = _("List items")

class FooterListItemLinkModel(AbstractLink):
    icon = Icon()
    text = models.CharField(verbose_name=_("Text"), max_length=64)

    class Meta:
        verbose_name = _("List item")
        verbose_name_plural = _("List items")

class FooterPageLinkListModel(CMSPlugin):
    pass

class FooterPageLinkItemModel(AbstractLink):
    pass

class FooterSocialIconListModel(CMSPlugin):
    pass

class FooterSocialIconItemModel(CMSPlugin):
    icon = Icon()
    link = models.URLField(verbose_name="Social url")

    class Meta:
        verbose_name = _("Social icon")
        verbose_name_plural = _("Social icons")

class SectionModel(CMSPlugin):
    """
    Layout > Grid: "Container" Plugin
    https://getbootstrap.com/docs/4.0/layout/grid/
    """
    tag_type = TagTypeField()
    attributes = AttributesField()

    class Meta:
        verbose_name = _("Section")
        verbose_name_plural = _("Sections")

    def __str__(self):
        return str(self.pk)

    def get_short_description(self):
        return '(Section {})'.format(str(self.pk))


class ColumnLayoutRow(models.Model):
    name = models.CharField(max_length=255)
    order = models.PositiveIntegerField(default=0)
    placeholder = models.ManyToManyField(Bootstrap4GridColumn)
    
    class Meta:
        verbose_name = _("Column layout")
        verbose_name_plural = _("Column layouts")
        ordering = ('order', 'name')
    
    def __str__(self):
        return self.name