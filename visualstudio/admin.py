from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from .models import ColumnLayoutRow
from djangocms_bootstrap4.contrib.bootstrap4_grid.models import Bootstrap4GridColumn
from djangocms_bootstrap4.contrib.bootstrap4_grid.forms import Bootstrap4GridColumnForm
from djangocms_bootstrap4.constants import DEVICE_SIZES

class Bootstrap4GridColumnAdmin(admin.ModelAdmin):
    form = Bootstrap4GridColumnForm
    fieldsets = [
        (None, {
            'fields': (
                ('column_type', 'column_alignment'),
            )
        }),
        (_('Responsive settings'), {
            'classes': ('wide',),
            'fields': (
                ('xs_col', 'sm_col', 'md_col', 'lg_col', 'xl_col'),
                ('xs_order', 'sm_order', 'md_order', 'lg_order', 'xl_order'),
                ('xs_offset', 'sm_offset', 'md_offset', 'lg_offset', 'xl_offset'),
                ('xs_ml', 'sm_ml', 'md_ml', 'lg_ml', 'xl_ml'),
                ('xs_mr', 'sm_mr', 'md_mr', 'lg_mr', 'xl_mr'),
            )
        }),
        (_('Advanced settings'), {
            'classes': ('collapse',),
            'fields': (
                'tag_type',
                'attributes',
            )
        }),
    ]
    

admin.site.register(Bootstrap4GridColumn, Bootstrap4GridColumnAdmin)

class ColumnLayoutRowAdmin(admin.ModelAdmin):
    pass

admin.site.register(ColumnLayoutRow, ColumnLayoutRowAdmin)