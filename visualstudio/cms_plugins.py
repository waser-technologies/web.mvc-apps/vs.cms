from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import gettext_lazy as _
from djangocms_bootstrap4.contrib.bootstrap4_grid.models import Bootstrap4GridContainer, Bootstrap4GridColumn
from djangocms_bootstrap4.contrib.bootstrap4_grid.cms_plugins import Bootstrap4GridRowPlugin, Bootstrap4GridColumnPlugin, Bootstrap4GridContainerPlugin
from djangocms_bootstrap4.helpers import get_plugin_template
from djangocms_bootstrap4.constants import DEVICE_SIZES

from . import models
from . import forms

@plugin_pool.register_plugin
class NavbarItemPlugin(CMSPluginBase):
    name = _('Navbar item')
    module = _('Navbar')
    model = models.LinkItem
    form = forms.LinkItemForm
    render_template = 'visualstudio/plugins/navbar_item.html'
    cache = True
    allow_children = False

    def render(self, context, instance, placeholder):
        context = super(NavbarItemPlugin, self).render(context, instance, placeholder)
        context['link'] = instance.get_link()
        return context

@plugin_pool.register_plugin
class FeatureOverlapButtonPlugin(CMSPluginBase):
    name = _('Feature overlap button')
    module = _('Feature')
    model = models.LinkItem
    form = forms.LinkItemForm
    render_template = 'visualstudio/plugins/feature_overlap_button_plugin.html'
    cache = True
    allow_children = False

    def render(self, context, instance, placeholder):
        context = super(FeatureOverlapButtonPlugin, self).render(context, instance, placeholder)
        context['link'] = instance.get_link()
        return context

@plugin_pool.register_plugin
class FeatureOverlapPlugin(CMSPluginBase):
    name = _('Feature overlap')
    module = _('Feature')
    model = CMSPlugin
    render_template = 'visualstudio/plugins/feature_overlap_plugin.html'
    cache = True
    allow_children = True
    child_classes = ['TextPlugin', 'FeatureOverlapButtonPlugin']

@plugin_pool.register_plugin
class BackgroundImagePlugin(CMSPluginBase):
    name = _('Background Image')
    module = _('Feature')
    model = models.BackgroundImage
    render_template = 'visualstudio/plugins/bg_img.html'
    cache = True
    allow_children = True
    child_classes = ['FeatureItemPlugin', 'FeatureItemCenterdPlugin']

@plugin_pool.register_plugin
class FeatureItemPlugin(CMSPluginBase):
    name = _('Feature item')
    module = _('Feature')
    model = CMSPlugin
    render_template = 'visualstudio/plugins/bg_img_content.html'
    cache = True
    allow_children = True
    require_parent = True
    parent_classes = ['BackgroundImagePlugin']

@plugin_pool.register_plugin
class FeatureItemCenterdPlugin(CMSPluginBase):
    name = _('Feature item centered')
    module = _('Feature')
    model = CMSPlugin
    render_template = 'visualstudio/plugins/bg_img_content_center.html'
    cache = True
    allow_children = True
    require_parent = True
    parent_classes = ['BackgroundImagePlugin']


@plugin_pool.register_plugin
class SidebarItemPlugin(CMSPluginBase):
    model = CMSPlugin
    name = _("Sidebar item")
    module = _("Sidebar")
    render_template = "visualstudio/plugins/sidebar_item_plugin.html"
    cache = False
    allow_children = True

plugin_pool.unregister_plugin(Bootstrap4GridColumnPlugin)

@plugin_pool.register_plugin
class ColumnPlugin(Bootstrap4GridColumnPlugin):
    pass #child_classes = ['RowPlugin', 'TextPlugin']

plugin_pool.unregister_plugin(Bootstrap4GridRowPlugin)

@plugin_pool.register_plugin
class RowPlugin(Bootstrap4GridRowPlugin):
    form = forms.RowPluginForm
    child_classes = ['ColumnPlugin']

    fieldsets = [
        (None, {
            'fields': (
                'layout',
                ('vertical_alignment', 'horizontal_alignment'),
            )
        }),
        (_('Advanced settings'), {
            'classes': ('collapse',),
            'fields': (
                ('tag_type', 'gutters',),
                'attributes',
            )
        }),
    ]

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        data = form.cleaned_data
        if data['layout'] is not None:
            for column in data['layout'].placeholder.all():
                extra = {}
                for size in DEVICE_SIZES:
                    extra['{}_col'.format(size)] = getattr(column, '{}_col'.format(size)) 
                
                col = Bootstrap4GridColumn(
                    parent=obj,
                    placeholder=obj.placeholder,
                    language=obj.language,
                    position=obj.numchild,
                    plugin_type=ColumnPlugin.__name__,
                    **extra
                )
                obj.add_child(instance=col)




@plugin_pool.register_plugin
class ContainerPlugin(Bootstrap4GridContainerPlugin):
    child_classes = ['RowPlugin']

plugin_pool.unregister_plugin(Bootstrap4GridContainerPlugin)

@plugin_pool.register_plugin
class SectionPlugin(CMSPluginBase):
    name = _("Section")
    render_template = "visualstudio/plugins/section_plugin.html"
    form = forms.SectionPluginForm
    model = models.SectionModel
    allow_children = True
    child_classes = ['ContainerPlugin', 'RowPlugin']

    fieldsets = [
        (None, {
            'fields': (
                'margin',
                'inverted',
            )
        }),
        (_('Advanced settings'), {
            'classes': ('collapse',),
            'fields': (
                'tag_type',
                'attributes',
            )
        }),
    ]

    def save_model(self, request, obj, form, change):
        response = super(SectionPlugin, self).save_model(request, obj, form, change)
        data = form.cleaned_data
        if data['margin'] == True:
            container = Bootstrap4GridContainer(
                parent=obj,
                placeholder=obj.placeholder,
                language=obj.language,
                position=obj.numchild,
                plugin_type=ContainerPlugin.__name__,
            )
            obj.add_child(instance=container)
        
        return response

@plugin_pool.register_plugin
class FooterListPlugin(CMSPluginBase):
    render_template = 'visualstudio/plugins/footer_list.html'
    name = _("List with icons")
    module = _("Footer")
    model = CMSPlugin
    allow_children = True
    child_classes = ['FooterListItemPlugin', 'FooterListLinkItemPlugin']

    def render(self, context, instance, placeholder):
        context = super(FooterListPlugin, self).render(context, instance, placeholder)
        return context

@plugin_pool.register_plugin
class FooterListItemPlugin(CMSPluginBase):
    render_template = 'visualstudio/plugins/footer_list_item.html'
    name = _('List item')
    module = _("Footer")
    model = models.FooterListItemModel
    form = forms.FooterListItemForm
    allow_children = False
    require_parent = True
    parent_classes = ['FooterListPlugin']

    def render(self, context, instance, placeholder):
        context = super(FooterListItemPlugin, self).render(context, instance, placeholder)
        return context

@plugin_pool.register_plugin
class FooterListLinkItemPlugin(CMSPluginBase):
    render_template = 'visualstudio/plugins/footer_list_item_link.html'
    name = _('List item with link')
    module = _("Footer")
    model = models.FooterListItemLinkModel
    form = forms.FooterListItemLinkForm
    allow_children = False
    require_parent = True
    parent_classes = ['FooterListPlugin']

    def render(self, context, instance, placeholder):
        context = super(FooterListLinkItemPlugin, self).render(context, instance, placeholder)
        context['link'] = instance.get_link()
        return context

@plugin_pool.register_plugin
class FooterPageLinkListPlugin(CMSPluginBase):
    render_template = 'visualstudio/plugins/footer_page_link_list.html'
    name = _("Page link list")
    module = _("Footer")
    model = models.FooterPageLinkListModel
    allow_children = True
    child_classes = ['FooterPageLinkItemPlugin']

    def render(self, context, instance, placeholder):
        context = super(FooterPageLinkListPlugin, self).render(context, instance, placeholder)
        return context

@plugin_pool.register_plugin
class FooterPageLinkItemPlugin(CMSPluginBase):
    render_template = 'visualstudio/plugins/footer_page_link_item.html'
    name = _('Page link item')
    module = _("Footer")
    model = models.FooterPageLinkItemModel
    form = forms.FooterPageLinkItemForm
    allow_children = False
    require_parent = True
    parent_classes = ['FooterPageLinkListPlugin']

    def render(self, context, instance, placeholder):
        context = super(FooterPageLinkItemPlugin, self).render(context, instance, placeholder)
        context['link'] = instance.get_link()
        return context

@plugin_pool.register_plugin
class FooterSocialIconListPlugin(CMSPluginBase):
    render_template = 'visualstudio/plugins/footer_social_icon_list.html'
    name = _("Social icons list")
    module = _("Footer")
    model = models.FooterSocialIconListModel
    allow_children = True
    child_classes = ['FooterSocialIconItemPlugin']

    def render(self, context, instance, placeholder):
        context = super(FooterSocialIconListPlugin, self).render(context, instance, placeholder)
        return context

@plugin_pool.register_plugin
class FooterSocialIconItemPlugin(CMSPluginBase):
    render_template = 'visualstudio/plugins/footer_social_icon_item.html'
    name = _('Social icons item')
    module = _("Footer")
    model = models.FooterSocialIconItemModel
    form = forms.FooterSocialIconItemForm
    allow_children = False
    require_parent = True
    parent_classes = ['FooterSocialIconListPlugin']

    def render(self, context, instance, placeholder):
        context = super(FooterSocialIconItemPlugin, self).render(context, instance, placeholder)
        return context