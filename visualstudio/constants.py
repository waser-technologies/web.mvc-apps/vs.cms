from django.conf import settings
from django.utils.translation import gettext_lazy as _
from djangocms_bootstrap4.constants import TAG_CHOICES

SECTION_DEFAULT_TAG = TAG_CHOICES[1][0]

SECTION_DEFAULT_ATTRIUTES = getattr(
    settings,
    'SECTION_DEFAULT_ATTRIUTES',
    {
        'class': "section py-4"
    },
)

SECTION_INVERTED_ATTRIUTES = getattr(
    settings,
    'SECTION_INVERTED_ATTRIUTES',
    {
        'class': "section section-inverted py-4"
    },
)